/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mvc.model;

/**
 *
 * @author TRANTIEN
 */
public class CTDT {
    public String maCTDT;
    public String tenCTDT;
    public String phienbanCTDT;

    public CTDT() {
        super();
    }

    public CTDT(String maCTDT, String tenCTDT, String phienbanCTDT) {
        this.maCTDT = maCTDT;
        this.tenCTDT = tenCTDT;
        this.phienbanCTDT = phienbanCTDT;
    }

    public String getMaCTDT() {
        return maCTDT;
    }

    public void setMaCTDT(String maCTDT) {
        this.maCTDT = maCTDT;
    }

    public String getTenCTDT() {
        return tenCTDT;
    }

    public void setTenCTDT(String tenCTDT) {
        this.tenCTDT = tenCTDT;
    }

    public String getPhienbanCTDT() {
        return phienbanCTDT;
    }

    public void setPhienbanCTDT(String phienbanCTDT) {
        this.phienbanCTDT = phienbanCTDT;
    }
    
    
    
}
