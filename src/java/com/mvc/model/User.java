/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mvc.model;

/**
 *
 * @author TRANTIEN
 */
public class User {
    private String mscb;
    private String password;
    private String hoten;
    public boolean valid;
    public User() {
        super();
    }

    public User(String mscb, String password) {
        this.mscb = mscb;
        this.password = password;
        this.hoten = hoten;
        this.valid = valid;
    }

    public String getMscb() {
        return mscb;
    }

    public void setMscb(String mscb) {
        this.mscb = mscb;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
    
    
    
}
