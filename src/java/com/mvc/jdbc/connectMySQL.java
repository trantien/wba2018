/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mvc.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
/**
 *
 * @author TRANTIEN
 */
public class connectMySQL {
    public static Connection getConnectionMySQL() throws SQLException ,ClassNotFoundException {
        String DB_URL = "jdbc:mysql://localhost:3306/chuongtrinhdaotao?characterEncoding=utf8";
        String USER_NAME = "root";
        String PASSWORD = "";
      return getConnectionMySQL(DB_URL, USER_NAME, PASSWORD);
  }
    
    public static Connection getConnectionMySQL(String dbURL, String userName,String password) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        try {
            
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(dbURL, userName, password);
        } catch (Exception ex) {
            System.out.println("connect failure!");
            ex.printStackTrace();
        }
        return conn;
    }
}
