/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mvc.dao;

import com.mvc.jdbc.connectMySQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.mvc.model.CTDT;

/**
 *
 * @author TRANTIEN
 */
public class CTDTDAO {
    private static Connection conn;
    private static PreparedStatement pstmt;
    private static ResultSet rs;
    static Statement stmt;

    public CTDTDAO() {
        super();
    }
    
    private static Connection getConnection() throws SQLException, ClassNotFoundException {
        Connection con = connectMySQL.getConnectionMySQL();
        return con;
    }
    public static String loadCTDT(){
        
      String strHTML="";
        try {
            
            conn = getConnection();
            stmt = conn.createStatement();
            //stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM chuongtrinhdaotao");
            if (rs.next()) {
                strHTML+="<tr>\n" +
                        "<td>"+rs.getString("ma_ctdt")+"</td>\n" +
                        "<td>"+rs.getString("ten_ctdt")+"</td>\n" +
                        "<td>"+rs.getString("phienban")+"</td>\n" +
                            "<td><a class='badge badge-success' href='"+"ChucNang.jsp?id_ctdt="+rs.getString("ma_ctdt")+"'>Chọn chương trình</a></td>\n" +
"		    </tr>";
             
            }
           rs.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.getMessage(); //Return string vể ajax đưa ra cảnh báo lỗi SQL cho Client
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();//Phần này liên quan tới insert nhiều lần bằng vòng lặp tôi chưa đề cập
                }
                if (stmt != null) {
                    stmt.close();//Phần này liên quan tới conn.rollback() về ban đầu coi như không có lần thực hiện nào với Database thành công tôi chưa đề cập
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return strHTML;
    }
}
