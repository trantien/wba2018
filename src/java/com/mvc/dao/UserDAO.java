/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.mvc.jdbc.connectMySQL;
import com.mvc.model.User;

/**
 *
 * @author TRANTIEN
 */
public class UserDAO {
    private static Connection conn;
    private static PreparedStatement pstmt;
    private static ResultSet rs;
    Statement stmt;
    private static Connection getConnection() throws SQLException, ClassNotFoundException {
        Connection con = connectMySQL.getConnectionMySQL();
        return con;
    }
    public static User login(User user){
        Statement stmt = null;    
	
        String mscb = user.getMscb();
        String password = user.getPassword();
        String searchQuery =
               "select * from taikhoan where masocb='"
                        + mscb
                        + "' AND matkhau='"
                        + password
                        + "'";
      System.out.println("Your user name is " + mscb);          
      System.out.println("Your password is " + password);
      System.out.println("Query: "+searchQuery);
      try{
          conn = getConnection();
          stmt = conn.createStatement();
          rs = stmt.executeQuery(searchQuery);
          boolean more = rs.next();
          if (!more) 
         {
            System.out.println("Không tìm thấy thông tin user. Vui lòng kiểm tra lại");
            user.setValid(false);
         }
         else if (more) 
         {
            String hoten = rs.getString("hoten");
	     	
            System.out.println("Welcome " + hoten);
            user.setHoten(hoten);
            user.setValid(true);
         }
          
          
      }
       catch (Exception ex) 
      {
         System.out.println("Log In failed: An Exception has occurred! " + ex);
      }
      finally 
      {
         if (rs != null){
            try {
               rs.close();
            } catch (Exception e) {}
               rs = null;
            }
	
         if (stmt != null) {
            try {
               stmt.close();
            } catch (Exception e) {}
               stmt = null;
            }
	
         if (stmt != null) {
            try {
               stmt.close();
            } catch (Exception e) {
            }

            stmt = null;
         }
      }
      
      
      return user;
    }
}
