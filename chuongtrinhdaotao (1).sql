-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 24, 2018 lúc 05:53 AM
-- Phiên bản máy phục vụ: 10.1.34-MariaDB
-- Phiên bản PHP: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `chuongtrinhdaotao`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bacdaotao`
--

CREATE TABLE `bacdaotao` (
  `mabac_dt` varchar(50) NOT NULL,
  `tenbac_dt` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bomon`
--

CREATE TABLE `bomon` (
  `id_bomon` varchar(50) NOT NULL,
  `id_khoa` varchar(50) NOT NULL,
  `ten_bomon` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bomon_hp`
--

CREATE TABLE `bomon_hp` (
  `id_bomon` varchar(50) NOT NULL,
  `id_hp` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cachtinhdiem`
--

CREATE TABLE `cachtinhdiem` (
  `id_cachtinhdiem` varchar(50) NOT NULL,
  `noidung` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chuandaura`
--

CREATE TABLE `chuandaura` (
  `ma_cdr` varchar(50) NOT NULL,
  `ten_cdr` varchar(500) NOT NULL,
  `manhom_ktkn` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chuongtrinhdaotao`
--

CREATE TABLE `chuongtrinhdaotao` (
  `ma_ctdt` varchar(50) NOT NULL,
  `phienban` varchar(50) NOT NULL,
  `ten_ctdt` varchar(200) NOT NULL,
  `manganh` varchar(50) NOT NULL,
  `id_bomon` varchar(50) NOT NULL,
  `mabac_dt` varchar(50) NOT NULL,
  `mahe_dt` varchar(50) NOT NULL,
  `idphienbanctgd` varchar(50) NOT NULL,
  `cautruc_ctdt` varchar(1000) NOT NULL,
  `thoigian_dt` varchar(200) NOT NULL,
  `thongtinapdung` varchar(200) NOT NULL,
  `thongtinthamdinh_pheduyet` varchar(200) NOT NULL,
  `dinhkem` varchar(200) NOT NULL,
  `trangthaipheduyet` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `chuongtrinhdaotao`
--

INSERT INTO `chuongtrinhdaotao` (`ma_ctdt`, `phienban`, `ten_ctdt`, `manganh`, `id_bomon`, `mabac_dt`, `mahe_dt`, `idphienbanctgd`, `cautruc_ctdt`, `thoigian_dt`, `thongtinapdung`, `thongtinthamdinh_pheduyet`, `dinhkem`, `trangthaipheduyet`) VALUES
('1', '1.0', 'Hệ thống thông tin', '62480104 ', '3', '2', '4', '1', '90 TC cho người tốt nghiệp thạc sĩ; 120 TC cho người tốt nghiệp đại học', '3-4 năm', 'abcd', 'abcd', 'abcd', '1');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ctdt_chuandaura`
--

CREATE TABLE `ctdt_chuandaura` (
  `ma_ctdt` varchar(50) NOT NULL,
  `phienban` varchar(50) NOT NULL,
  `ma_cdr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ctdt_hocphanbskt`
--

CREATE TABLE `ctdt_hocphanbskt` (
  `ma_ctdt` varchar(50) NOT NULL,
  `phienban` varchar(50) NOT NULL,
  `id_hp` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ctdt_muctieu`
--

CREATE TABLE `ctdt_muctieu` (
  `ma_ctdt` varchar(50) NOT NULL,
  `phienban` varchar(50) NOT NULL,
  `mamuctieu` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ctdt_nganhdungphuhop`
--

CREATE TABLE `ctdt_nganhdungphuhop` (
  `ma_ctdt` varchar(50) NOT NULL,
  `phienban` varchar(50) NOT NULL,
  `manganh` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ctdt_nganhduthi`
--

CREATE TABLE `ctdt_nganhduthi` (
  `ma_ctdt` varchar(50) NOT NULL,
  `phienban` varchar(50) NOT NULL,
  `manganh` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ctdt_nganhgan`
--

CREATE TABLE `ctdt_nganhgan` (
  `ma_ctdt` varchar(50) NOT NULL,
  `phienban` varchar(50) NOT NULL,
  `manganh` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ctdt_tosoanthao`
--

CREATE TABLE `ctdt_tosoanthao` (
  `ma_ctdt` varchar(50) NOT NULL,
  `phienban` varchar(50) NOT NULL,
  `masocb` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `danhgiahocphan`
--

CREATE TABLE `danhgiahocphan` (
  `id_danhgiahp` varchar(50) NOT NULL,
  `diemthanhphan` varchar(200) NOT NULL,
  `quydinh` varchar(200) NOT NULL,
  `trongso` int(3) NOT NULL,
  `muctieu` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hedaotao`
--

CREATE TABLE `hedaotao` (
  `mahe_dt` varchar(50) NOT NULL,
  `tenhe_dt` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hocphan_muctieu`
--

CREATE TABLE `hocphan_muctieu` (
  `id_muctieu_hp` varchar(50) NOT NULL,
  `id_hp` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoc_phan`
--

CREATE TABLE `hoc_phan` (
  `id_hp` varchar(50) NOT NULL,
  `id_verson` varchar(50) NOT NULL,
  `ten_hp` varchar(200) NOT NULL,
  `ma_hp` varchar(50) NOT NULL,
  `so_tc` int(10) NOT NULL,
  `so_tiet` int(10) NOT NULL,
  `dk_tienquyet` varchar(200) NOT NULL,
  `tomtat_noidung` varchar(500) NOT NULL,
  `pp_giangday` varchar(500) NOT NULL,
  `nv_sinhvien` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hp_cachtinhdiem_hp`
--

CREATE TABLE `hp_cachtinhdiem_hp` (
  `id_hp` varchar(50) NOT NULL,
  `id_cachtinhdiem` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hp_danhgia_hp`
--

CREATE TABLE `hp_danhgia_hp` (
  `id_hp` varchar(50) NOT NULL,
  `id_danhgiahp` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khoa`
--

CREATE TABLE `khoa` (
  `id_khoa` varchar(50) NOT NULL,
  `ten_khoa` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khoinganh`
--

CREATE TABLE `khoinganh` (
  `makn` varchar(50) NOT NULL,
  `tenkn` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loaichuandaura`
--

CREATE TABLE `loaichuandaura` (
  `maloai_cdr` varchar(50) NOT NULL,
  `tenloai_cdr` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `muctieu_ctdt`
--

CREATE TABLE `muctieu_ctdt` (
  `mamuctieu` varchar(50) NOT NULL,
  `noidung` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `muctieu_hocphan`
--

CREATE TABLE `muctieu_hocphan` (
  `id_muctieu_hp` varchar(50) NOT NULL,
  `ma_muctieu_hp` varchar(10) NOT NULL,
  `ten_muctieu_hp` varchar(500) NOT NULL,
  `loai_muctieu_hp` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nganhdaotao`
--

CREATE TABLE `nganhdaotao` (
  `manganh` varchar(50) NOT NULL,
  `tennganh` varchar(200) NOT NULL,
  `makn` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nhomkienthuckynang`
--

CREATE TABLE `nhomkienthuckynang` (
  `manhom_ktkn` varchar(50) NOT NULL,
  `tennhom_ktkn` varchar(200) NOT NULL,
  `maloai_cdr` varchar(50) NOT NULL,
  `trangthaihienthi` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `noidunghocphan`
--

CREATE TABLE `noidunghocphan` (
  `id_nd_hp` varchar(50) NOT NULL,
  `id_hp` varchar(50) NOT NULL,
  `ma_nd_hp` varchar(200) NOT NULL,
  `ten_nd_hp` varchar(500) NOT NULL,
  `sotiet` int(3) NOT NULL,
  `nd_hp_parent` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `phienbanchuongtrinhgiangday`
--

CREATE TABLE `phienbanchuongtrinhgiangday` (
  `ma_ctdt` varchar(50) NOT NULL,
  `phienban` varchar(50) NOT NULL,
  `idphienbanctgd` varchar(50) NOT NULL,
  `ngaybanhanh` varchar(200) NOT NULL,
  `khoikienthuc` varchar(200) NOT NULL,
  `id_hp` varchar(50) NOT NULL,
  `ten_hp` varchar(200) NOT NULL,
  `so_tc` int(10) NOT NULL,
  `nhom_batbuoc` varchar(200) NOT NULL,
  `nhom_tuchon` varchar(200) NOT NULL,
  `so_tiet_lt` int(10) NOT NULL,
  `so_tiet_th` int(10) NOT NULL,
  `dk_tienquyet` varchar(200) NOT NULL,
  `hk_thuchien` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `taikhoan`
--

CREATE TABLE `taikhoan` (
  `masocb` varchar(50) NOT NULL,
  `matkhau` varchar(50) NOT NULL,
  `hoten` varchar(200) NOT NULL,
  `gioitinh` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `id_khoa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `taikhoan`
--

INSERT INTO `taikhoan` (`masocb`, `matkhau`, `hoten`, `gioitinh`, `email`, `id_khoa`) VALUES
('ctu-001', '123456', 'Trần Văn Tiên', '1', 'trantien@ctu.edu.vn', '1');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `version`
--

CREATE TABLE `version` (
  `id_version` varchar(50) NOT NULL,
  `version` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bacdaotao`
--
ALTER TABLE `bacdaotao`
  ADD PRIMARY KEY (`mabac_dt`);

--
-- Chỉ mục cho bảng `bomon`
--
ALTER TABLE `bomon`
  ADD PRIMARY KEY (`id_bomon`);

--
-- Chỉ mục cho bảng `bomon_hp`
--
ALTER TABLE `bomon_hp`
  ADD PRIMARY KEY (`id_bomon`);

--
-- Chỉ mục cho bảng `cachtinhdiem`
--
ALTER TABLE `cachtinhdiem`
  ADD PRIMARY KEY (`id_cachtinhdiem`);

--
-- Chỉ mục cho bảng `chuandaura`
--
ALTER TABLE `chuandaura`
  ADD PRIMARY KEY (`ma_cdr`);

--
-- Chỉ mục cho bảng `chuongtrinhdaotao`
--
ALTER TABLE `chuongtrinhdaotao`
  ADD PRIMARY KEY (`ma_ctdt`,`phienban`);

--
-- Chỉ mục cho bảng `ctdt_chuandaura`
--
ALTER TABLE `ctdt_chuandaura`
  ADD PRIMARY KEY (`ma_ctdt`,`phienban`,`ma_cdr`);

--
-- Chỉ mục cho bảng `ctdt_hocphanbskt`
--
ALTER TABLE `ctdt_hocphanbskt`
  ADD PRIMARY KEY (`ma_ctdt`,`phienban`,`id_hp`);

--
-- Chỉ mục cho bảng `ctdt_muctieu`
--
ALTER TABLE `ctdt_muctieu`
  ADD PRIMARY KEY (`ma_ctdt`,`phienban`,`mamuctieu`);

--
-- Chỉ mục cho bảng `ctdt_nganhdungphuhop`
--
ALTER TABLE `ctdt_nganhdungphuhop`
  ADD PRIMARY KEY (`ma_ctdt`,`phienban`,`manganh`);

--
-- Chỉ mục cho bảng `ctdt_nganhduthi`
--
ALTER TABLE `ctdt_nganhduthi`
  ADD PRIMARY KEY (`ma_ctdt`,`phienban`,`manganh`);

--
-- Chỉ mục cho bảng `ctdt_nganhgan`
--
ALTER TABLE `ctdt_nganhgan`
  ADD PRIMARY KEY (`ma_ctdt`,`phienban`,`manganh`);

--
-- Chỉ mục cho bảng `ctdt_tosoanthao`
--
ALTER TABLE `ctdt_tosoanthao`
  ADD PRIMARY KEY (`ma_ctdt`,`phienban`,`masocb`);

--
-- Chỉ mục cho bảng `danhgiahocphan`
--
ALTER TABLE `danhgiahocphan`
  ADD PRIMARY KEY (`id_danhgiahp`);

--
-- Chỉ mục cho bảng `hedaotao`
--
ALTER TABLE `hedaotao`
  ADD PRIMARY KEY (`mahe_dt`);

--
-- Chỉ mục cho bảng `hocphan_muctieu`
--
ALTER TABLE `hocphan_muctieu`
  ADD PRIMARY KEY (`id_muctieu_hp`);

--
-- Chỉ mục cho bảng `hoc_phan`
--
ALTER TABLE `hoc_phan`
  ADD PRIMARY KEY (`id_hp`);

--
-- Chỉ mục cho bảng `hp_cachtinhdiem_hp`
--
ALTER TABLE `hp_cachtinhdiem_hp`
  ADD PRIMARY KEY (`id_hp`,`id_cachtinhdiem`);

--
-- Chỉ mục cho bảng `hp_danhgia_hp`
--
ALTER TABLE `hp_danhgia_hp`
  ADD PRIMARY KEY (`id_hp`,`id_danhgiahp`);

--
-- Chỉ mục cho bảng `khoa`
--
ALTER TABLE `khoa`
  ADD PRIMARY KEY (`id_khoa`);

--
-- Chỉ mục cho bảng `khoinganh`
--
ALTER TABLE `khoinganh`
  ADD PRIMARY KEY (`makn`);

--
-- Chỉ mục cho bảng `loaichuandaura`
--
ALTER TABLE `loaichuandaura`
  ADD PRIMARY KEY (`maloai_cdr`);

--
-- Chỉ mục cho bảng `muctieu_ctdt`
--
ALTER TABLE `muctieu_ctdt`
  ADD PRIMARY KEY (`mamuctieu`);

--
-- Chỉ mục cho bảng `muctieu_hocphan`
--
ALTER TABLE `muctieu_hocphan`
  ADD PRIMARY KEY (`id_muctieu_hp`);

--
-- Chỉ mục cho bảng `nganhdaotao`
--
ALTER TABLE `nganhdaotao`
  ADD PRIMARY KEY (`manganh`);

--
-- Chỉ mục cho bảng `nhomkienthuckynang`
--
ALTER TABLE `nhomkienthuckynang`
  ADD PRIMARY KEY (`manhom_ktkn`);

--
-- Chỉ mục cho bảng `noidunghocphan`
--
ALTER TABLE `noidunghocphan`
  ADD PRIMARY KEY (`id_nd_hp`);

--
-- Chỉ mục cho bảng `phienbanchuongtrinhgiangday`
--
ALTER TABLE `phienbanchuongtrinhgiangday`
  ADD PRIMARY KEY (`ma_ctdt`,`phienban`,`idphienbanctgd`,`id_hp`);

--
-- Chỉ mục cho bảng `taikhoan`
--
ALTER TABLE `taikhoan`
  ADD PRIMARY KEY (`masocb`);

--
-- Chỉ mục cho bảng `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`id_version`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
