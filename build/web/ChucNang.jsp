<%-- 
    Document   : trangchu
    Created on : Oct 23, 2018, 1:35:24 PM
    Author     : TRANTIEN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Hệ thống quản lý đào tạo - CTU</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="shortcut icon" href="images/favicon.png"> 


    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    </head>
    
    <body>
    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="index.html"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                    </li>
                    <li class="menu-title">UI elements</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>Components</a>
                        <ul class="sub-menu children dropdown-menu">                            <li><i class="fa fa-puzzle-piece"></i><a href="ui-buttons.html">Buttons</a></li>
                            <li><i class="fa fa-id-badge"></i><a href="ui-badges.html">Badges</a></li>
                            <li><i class="fa fa-bars"></i><a href="ui-tabs.html">Tabs</a></li>
                            
                            <li><i class="fa fa-id-card-o"></i><a href="ui-cards.html">Cards</a></li>
                            <li><i class="fa fa-exclamation-triangle"></i><a href="ui-alerts.html">Alerts</a></li>
                            <li><i class="fa fa-spinner"></i><a href="ui-progressbar.html">Progress Bars</a></li>
                            <li><i class="fa fa-fire"></i><a href="ui-modals.html">Modals</a></li>
                            <li><i class="fa fa-book"></i><a href="ui-switches.html">Switches</a></li>
                            <li><i class="fa fa-th"></i><a href="ui-grids.html">Grids</a></li>
                            <li><i class="fa fa-file-word-o"></i><a href="ui-typgraphy.html">Typography</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Tables</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="tables-basic.html">Basic Table</a></li>
                            <li><i class="fa fa-table"></i><a href="tables-data.html">Data Table</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Forms</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a></li>
                        </ul>
                    </li>

                    <li class="menu-title">Icons</li><!-- /.menu-title -->

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Icons</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-fort-awesome"></i><a href="font-fontawesome.html">Font Awesome</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="font-themify.html">Themefy Icons</a></li>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="widgets.html"> <i class="menu-icon ti-email"></i>Widgets </a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Charts</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-line-chart"></i><a href="charts-chartjs.html">Chart JS</a></li>
                            <li><i class="menu-icon fa fa-area-chart"></i><a href="charts-flot.html">Flot Chart</a></li>
                            <li><i class="menu-icon fa fa-pie-chart"></i><a href="charts-peity.html">Peity Chart</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Maps</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-map-o"></i><a href="maps-gmap.html">Google Maps</a></li>
                            <li><i class="menu-icon fa fa-street-view"></i><a href="maps-vector.html">Vector Maps</a></li>
                        </ul>
                    </li>
                    <li class="menu-title">Extras</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-login.html">Login</a></li>
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-register.html">Register</a></li>
                            <li><i class="menu-icon fa fa-paper-plane"></i><a href="pages-forget.html">Forget Pass</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->


    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">  
            <div class="top-left">
                <div class="navbar-header"> 
                    <a class="navbar-brand" href="./"><img src="images/logo3.png" alt="Logo"></a>
                    <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a> 
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a> 
                </div> 
            </div>
            <div class="top-right"> 
                <div class="header-menu"> 
                        

                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="#"><strong>Trần Văn Tiên</strong></a>
                            <a class="nav-link" href="#"><i class="fa fa-user"></i>Thông tin cá nhân</a>
                            <a class="nav-link" href="#"><i class="fa fa-power-off"></i>Logout</a>
                        </div>
                    </div> 
                </div>  
            </div>
        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>Trang chủ</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Trang chủ</a></li>
                                    <li class="active">Chọn chức năng</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="animated fadeIn">
            
                <div class="row"> 
                    <div class="col-sm-6 col-lg-6">
                        <div class="card text-white bg-flat-color-1">
                            <div class="card-body">
                                <div class="pt-1">
                                    <h3 class="mb-0 fw-r">
                                        <span>Mục tiêu - Chuẩn đầu ra</span>
                                    </h3>
                                    <p class="text-light mt-1 m-0">Ma trận Mối quan hệ giữa mục tiêu đào tạo với chuẩn đầu ra</p>
                                </div><!-- /.card-left -->

                            </div>
                            <div class="card-body card-block">
                                <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col-12 col-md-12 mb-15">
                                        <select name="select" id="select" class="form-control form-control-lg">
                                            <option value="0">Chọn mục tiêu</option>
                                            <option value="1">1A - Có kiến thức ...</option>
                                            <option value="2">1B - Có trình độ ...</option>
                                            <option value="3">1C - Có khả năng ...</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-12">
                                    <button type="button" class="btn btn-lg btn-block btn-outline-light mr-1">Thêm mới</button>
                                    <button type="button" class="btn btn-lg btn-block btn-outline-light mr-1">Chỉnh sửa</button>
                                    <button type="button" class="btn btn-lg btn-block btn-outline-light mr-1">Xóa</button>
                                    <button type="button" class="btn btn-lg btn-block btn-outline-light mr-1">Xem ma trận</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                            

                        </div>
                    </div>
                    <!--/.col-->
                    <div class="col-sm-6 col-lg-6">
                        <div class="card text-white bg-flat-color-2">
                            <div class="card-body">
                                <div class="pt-1">
                                    <h3 class="mb-0 fw-r">
                                        <span>Học phần - Chuẩn đầu ra</span>
                                    </h3>
                                    <p class="text-light mt-1 m-0">Ma trận Mối quan hệ giữa học phần với chuẩn đầu ra</p>
                                </div><!-- /.card-left -->

                            </div>
                            <div class="card-body card-block">
                                <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col-12 col-md-12 mb-15">
                                        <select name="select" id="select" class="form-control form-control-lg">
                                            <option value="0">Chọn học phần</option>
                                            <option value="1">CT-101 ...</option>
                                            <option value="2">CT-102 ...</option>
                                            <option value="3">CT-103 ...</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-12">
                                    <button type="button" class="btn btn-lg btn-block btn-outline-light mr-1">Thêm mới</button>
                                    <button type="button" class="btn btn-lg btn-block btn-outline-light mr-1">Chỉnh sửa</button>
                                    <button type="button" class="btn btn-lg btn-block btn-outline-light mr-1">Xóa</button>
                                    <button type="button" class="btn btn-lg btn-block btn-outline-light mr-1">Xem ma trận</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                           

                        </div>
                    </div>
                    <!--/.col-->

                  <!--  <div class="col-sm-6 col-lg-6">
                        <div class="card text-white bg-flat-color-6">
                            <div class="card-body">
                                <div class="pt-1">
                                    <h3 class="mb-0 fw-r">
                                        <span>Học phần - Chuẩn đầu ra</span>
                                    </h3>
                                    <p class="text-light mt-1 m-0">Mối quan hệ giữa học phần với chuẩn đầu ra</p>
                                </div><!-- /.card-left -->
                       <!--         <div class="card-body">
                                 <button type="button" class="btn btn-lg btn-block">Thêm mới</button>
                                 <button type="button" class="btn btn-lg btn-block">Chỉnh sửa</button>
                                 <button type="button" class="btn btn-lg btn-block">Tìm kiếm</button>
                                </div>


                            </div>

                        </div>
                    </div>
                    <!--/.col-->


                </div><!-- .row -->
                
            </div><!-- .animated -->
        </div><!-- .content -->


        <div class="clearfix"></div>

        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Đồ án WBA 2018
                    </div>
                    <div class="col-sm-6 text-right">
                        Nhóm thực hiện: Trần Văn Tiên / 
                        Trần Văn Út Chính / 
                        Nguyễn Tấn Phú
                    </div>
                </div>
            </div>
        </footer>


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>

    <!--  Chart js -->
    <script src="assets/js/lib/chart-js/Chart.bundle.js"></script>

    <!--Flot Chart-->

    <script src="assets/js/lib/flot-chart/jquery.flot.js"></script>
    <script src="assets/js/lib/flot-chart/jquery.flot.spline.js"></script>

    <script src="assets/js/widgets.js"></script>

</body>
</html>
